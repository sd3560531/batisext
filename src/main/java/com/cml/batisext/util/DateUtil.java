package com.cml.batisext.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static String date2String(Date date){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
	}
	
	public static String dateTime2String(Date date){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(date);
	}
	
	public static Date str2Date(String str,String format) throws ParseException{
		DateFormat df = new SimpleDateFormat("format");
		return df.parse(str);
	}
	
	public static String getCurrentDateTimeStr(){
		return dateTime2String(new Date());
	}
	
	public static String getCurrentDateStr(){
		return date2String(new Date());
	}
	
	public static Date getCurrentDate(){
		try {
			return str2Date(getCurrentDateStr(),"yyyy-MM-dd");
		} catch (Exception e) {
			return null;
		}
	}
	
}
