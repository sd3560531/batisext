package com.cml.batisext.demo;

import org.apache.ibatis.annotations.Mapper;

import com.cml.batisext.core.bean.Demo;
import com.cml.batisext.core.repository.mybatis.annotations.GenerateDao;
import com.cml.batisext.core.repository.mybatis.base.BaseDao;


@Mapper
@GenerateDao(beanType = Demo.class)
public interface DemoDao extends BaseDao<Demo>{
	
}
