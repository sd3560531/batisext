package com.cml.batisext.core.exception;

/**
 * 当更新时，传入version与要更新的实体的version不一致，抛出此异常
 * @author lwh
 *
 */
public class DataHaveChangedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	
	
	public DataHaveChangedException() {
		super("data already modify!");
	}
	
}
