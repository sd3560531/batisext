package com.cml.batisext.core.exception;

/**
 * batisext配置等发生的异常
 * @author lwh
 */
public class BatisExtException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BatisExtException() {
		super();
	}

	public BatisExtException(String message) {
		super(message);
	}
}
