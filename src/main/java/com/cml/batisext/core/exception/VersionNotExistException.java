package com.cml.batisext.core.exception;

/**
 * 
 * @author lwh
 */
public class VersionNotExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	
	
	public VersionNotExistException() {
		super("Version Not Exist!");
	}
	
}
