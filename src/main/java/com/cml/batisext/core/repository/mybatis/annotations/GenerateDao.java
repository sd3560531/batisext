package com.cml.batisext.core.repository.mybatis.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.cml.batisext.core.bean.base.DatabaseBean;

/**
 * 该标注指定一个dao继承通用dao的特性
 * 
 * <br><br>
 * 主要用于将dao指定的具体泛型类型传递给mybatis拦截器
 * 
 * @author lwh
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GenerateDao {
	Class<? extends DatabaseBean> beanType();
}
