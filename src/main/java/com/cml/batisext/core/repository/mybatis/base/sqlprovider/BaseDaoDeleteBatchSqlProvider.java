package com.cml.batisext.core.repository.mybatis.base.sqlprovider;

import java.util.List;
import java.util.Map;

import com.cml.batisext.core.repository.mybatis.annotations.DbTable;
import com.cml.batisext.core.repository.mybatis.base.BaseDao;
import com.cml.batisext.core.repository.mybatis.base.BaseDaoSqlProvider;

/**
 * 通用接口的delete方法的sql生成器,目前仅适用于mysql
 * @author lwh
 *
 */
public class BaseDaoDeleteBatchSqlProvider extends BaseDaoSqlProvider {

    @Override
    public String doBuildSql(Map<String, Object> params, Class<?> beanType) throws Exception {

        String tableName = beanType.getAnnotation(DbTable.class).table();
        //        List<Field> fields = getAllDbField(beanType);
        //
        //        Class<?> tmpColumnType = null;
        //        Object fieldValue = null;
        //        String sqlValue = " ";
        String sqlValues = " ";
        String sql = "delete from " + tableName + " where id in ";

        List<Object> list = (List<Object>) params.get(BaseDao.LIST_PARAM_NAME);

        for (Object listobj : list) {
            Long pk = getPKValue(listobj);
            if (pk == null) {
                throw new Exception("传入bean中id字段必须有值");
            }
            sqlValues += pk + ",";
        }
        sql += "(" + sqlValues.substring(0, sqlValues.length() - 1) + ")";
        return sql;
    }
}
