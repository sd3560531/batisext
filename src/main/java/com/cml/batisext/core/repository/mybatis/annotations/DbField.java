package com.cml.batisext.core.repository.mybatis.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该标注指定实体里面哪些字段对应了数据库
 * <br><br>
 * 该标注主要用于在Sql provider中，判断实体字段是否对应数据库字段
 * <br><br>
 * 如果一个实体里的字段，在数据库中没有对应字段，<b>不</b>要给该字段加本标注
 * @author lwh
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DbField {
	/**
	 * 列名，当实体字段名与数据库字段名不一致时，指定该值
	 * @return String
	 */
	String column()default "";
	
	/**
	 * 列类型，当实体字段类型与数据库字段类型不一致时，指定该值
	 * @return
	 */
	Class<?> type()default Object.class;
}
