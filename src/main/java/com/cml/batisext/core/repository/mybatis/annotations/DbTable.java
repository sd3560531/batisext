package com.cml.batisext.core.repository.mybatis.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 本标注标记实体类对应的数据表
 * 
 * <br><br>
 * 主要用于在Sql provider中，根据实体获取表名，必须设置值
 * @author lwh
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DbTable {
	String table()default "";
}
