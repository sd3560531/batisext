package com.cml.batisext.core.bean;

import java.util.Date;

import com.cml.batisext.core.bean.base.DatabaseBean;
import com.cml.batisext.core.repository.mybatis.annotations.DbField;
import com.cml.batisext.core.repository.mybatis.annotations.DbTable;

@DbTable(table = "demo")
public class Demo extends DatabaseBean{

    private static final long serialVersionUID = 1L;

    @DbField
    private String name;
    
    @DbField
    private Integer age;
    
    @DbField
    private Date careateTime;

}
