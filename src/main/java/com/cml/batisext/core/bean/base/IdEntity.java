package com.cml.batisext.core.bean.base;

import java.io.Serializable;

import com.cml.batisext.core.repository.mybatis.annotations.DbField;

/**
 * 统一定义id的entity基类.
 * 
 * 基类统一定义id的属性名称、数据类型、列名映射及生成策略.
 * 
 * @author lwh
 */
public abstract class IdEntity implements Serializable {

	private static final long serialVersionUID = -1157475528840998144L;
	
	@DbField
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
