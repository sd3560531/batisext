package com.cml.batisext.core.bean.base;

import com.cml.batisext.core.repository.mybatis.annotations.DbField;

/**
 * 对应到数据库中表达实体，都需要继承该类
 * @author Administrator
 *
 */
public class DatabaseBean extends IdEntity {

	private static final long serialVersionUID = 1L;
	
	@DbField
	protected Integer version;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
