package com.cml.batisext.core.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cml.batisext.core.repository.mybatis.interceptor.BaseDaoInterceptor;

@Configuration
public class BatisExtConfiguration {

	public BatisExtConfiguration(){}
	
	@Bean
	public BaseDaoInterceptor baseDaoInterceptor(){
		return new BaseDaoInterceptor();
	}
}
